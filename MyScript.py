#!/usr/bin/python
import os

def installaIde():
        os.system("sudo apt-get install filezilla -y")
        os.system("sudo apt-get install fritzing -y")
        os.system("sudo apt-get install codeblocks -y")
        os.system("cd Scaricati && wget https://atom.io/download/deb")
        os.system("sudo dpkg -i atom-amd64.deb")
def pulisciSistema():
        os.system("sudo apt-get autoremove")
        os.system("sudo apt-get autoclean")
        os.system("sudo apt-get purge")
        os.system("sudo apt-get clean")
def riparaAggiornamenti():
        os.system("sudo apt-get check")
        os.system("sudo apt-get -f install ")
        os.system("sudo rm -r /var/lib/apt/lists/* -vf")
        os.system("sudo apt-get update")
        os.system("sudo apt-get upgrade")
def avanzaStabile():
        os.system("do-release-upgrade")
def avanzaSviluppo():
        os.system("do-release-upgrade -d")
def installJavaNodeVBgit():
	os.system("sudo apt-get purge openjdk*")
	os.system("sudo add-apt-repository ppa:webupd8team/java -y")
	os.system(" sudo apt-get update")
	os.system(" sudo apt-get install oracle-java8-installer -y")
	os.system("curl -sL https://deb.nodesource.com/setup | sudo bash -")
	os.system("sudo apt-get install nodejs -y")
	os.system("sudo apt-get install build-essential -y")
	os.system("sudo apt-get install virtualbox -y")
	os.system("sudo apt-get update")
	os.system("sudo apt-get install git -y")



def installNautilusExtension():
	os.system("sudo apt-get install nautilus-image-converter imagemagick -y")
	os.system("killall nautilus")
	os.system("sudo apt-get install nautilus-open-terminal -y")
	os.system("sudo add-apt-repository ppa:tualatrix/next -y")
	os.system("sudo apt-get update ")
	os.system("sudo apt-get install ubuntu-tweak -y")
	os.system("sudo add-apt-repository ppa:indicator-multiload/stable-daily -y")
	os.system("sudo apt-get install nautilus-open-terminal -y")
	os.system("sudo apt-get update")
	os.system("sudo apt-get install indicator-multiload")
	os.system("indicator-multiload")



risposta = input('''Scegli un'opzione:
           1- pulisci il sistema
           2- ripara aggiornamenti
           3- aggiorna il sistema ad una versione stabile
           4- aggiorna il sistema in una versione in sviluppo
           5- installa ide + filezilla
	   6- installa java8, node js , virtualbox e git
	   7- estensioni per nautilus, ubuntu-tweak e indicator multiload
           8- esci
           ''')
if risposta == 1:
        pulisciSistema()
elif risposta == 2:
        riparaAggiornamenti()
elif risposta == 3:
        avanzaStabile()
elif risposta == 4:
        avanzaSviluppo()
elif risposta == 5:
        installaIde()
elif risposta == 6:
	installJavaNodeVBgit()
elif risposta == 7:
	installNautilusExtension()
else:
        exit()
